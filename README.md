# OpenML dataset: walking-activity

https://www.openml.org/d/1509

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: P. Casale, O. Pujol, P. Radeva.    
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/User+Identification+From+Walking+Activity)  
**Please cite**: Casale, P. Pujol, O. and Radeva, P. 'Personalization and user verification in wearable systems using biometric walking patterns' Personal and Ubiquitous Computing, 16(5), 563-580, 2012

**User Identification From Walking Activity Data Set**  
The dataset collects data from an Android smartphone positioned in the chest pocket. Accelerometer Data are collected from 22 participants walking in the wild over a predefined path. The dataset is intended for Activity Recognition research purposes. It provides challenges for identification and authentication of people using motion patterns. 

**Note: the original per-user datasets were joined into one dataset**

### Attribute Information  
Time-step, x acceleration, y acceleration, z acceleration  
Target: User ID.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1509) of an [OpenML dataset](https://www.openml.org/d/1509). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1509/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1509/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1509/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

